package com.example.administrator.demo3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button bt,bt2;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt=(Button)findViewById(R.id.button);
        bt2=(Button)findViewById(R.id.button2);
        tv= (TextView) findViewById(R.id.textView);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hienthidialog();
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast t=new Toast(MainActivity.this);
                t.setGravity(Gravity.TOP | Gravity.LEFT, 200, 200);
                t.setDuration(Toast.LENGTH_LONG);

                LayoutInflater  inf=getLayoutInflater();
                View view=inf.inflate(R.layout.custom_toast, null);
                ImageView iv=(ImageView)view.findViewById(R.id.image);
                TextView tv=(TextView)view.findViewById(R.id.text);
                iv.setImageResource(R.mipmap.ic_launcher);
                tv.setText("tui la cai toast bi custom");

                t.setView(view);
                t.show();

            }
        });
    }

    public void hienthidialog()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inf=getLayoutInflater();
        final View view=inf.inflate(R.layout.custom_dialog,null);

        builder.setView(view);

        builder.setNegativeButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText et1=(EditText)view.findViewById(R.id.username);
                EditText et2=(EditText)view.findViewById(R.id.password);
                tv.setText("ten:"+ et1.getText().toString()+ "\n"+"pass:"+ et2.getText().toString());
                    
            }
        });
        builder.setPositiveButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        AlertDialog mydialog=builder.create();
        mydialog.show();

    }
}
